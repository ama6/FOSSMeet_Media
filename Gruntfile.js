module.exports = function(grunt) {

    require('load-grunt-tasks')(grunt);
    require('time-grunt')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jshint: {
            main: {
                files: [{
                    src: ['Gruntfile.js', 'js/**/*.js']
                }]
            },
            options: {
                globals: {
                    'jQuery': true,
                    'angular': true,
                    'console': true,
                    '$': true,
                    '_': true,
                    'moment': true
                }
            }
        },

        htmlhint: {
            options: {
                'attr-lower-case': true,
                'attr-value-not-empty': false,
                'tag-pair': true,
                'tag-self-close': true,
                'tagname-lowercase': true,
                'id-class-value': true,
                'id-unique': true,
                'img-alt-require': true,
                'img-alt-not-empty': true
            },
            main: {
                src: ['public/**.html']
            }
        },

        concat: {
            app: {
                src: ['js/**'],
                dest: 'public/js/app.js'
            },
            appstyles: {
                src: ['css/**'],
                dest: 'public/css/base.css'
            },
        },

        pug: {
            options: {
                pretty: true,
                data: {
                    debug: true
                }
            },
            pages: {
                files: [{
                    expand: true,
                    cwd: 'pug/pages',
                    src: '*.pug',
                    dest: 'public/',
                    ext: '.html'
                }]
            }
        },

        watch: {
            grunt: {
                files: ['Gruntfile.js'],
                options: {
                    nospawn: true,
                    keepalive: true,
                    livereload: true
                },
                tasks: ['public:dev']
            },
            html: {
                files: ['public/*.html'],
                options: {
                    nospawn: true,
                    keepalive: true,
                    livereload: true
                },
                tasks: ['htmlhint']
            },
            js: {
                files: 'js/**',
                options: {
                    nospawn: true,
                    livereload: true
                },
                tasks: ['jshint', 'concat']
            },
            pug: {
                files: ['pug/**'],
                options: {
                    nospawn: true,
                    livereload: true
                },
                tasks: ['pug']
            },
           css: {
               files: 'css/**',
               options: {
                   nospawn: true,
                   livereload: true
               },
               tasks: ['concat']
           }

        },

        connect: {
            server: {
                options: {
                    port: 8080,
                    base: 'public',
                    hostname: 'localhost',
                    livereload: true
                }
            }
        }

    });

    grunt.registerTask('public:dev', function(target) {
        grunt.task.run([
            'htmlhint',
            'jshint',
            'pug',
            'concat'
        ]);
    });

    grunt.registerTask('serve', function(target) {
        grunt.task.run([
            'public:dev',
            'connect',
            'watch'
        ]);
    });

};
